import copy

from django.template import engines

from django_cdstack_deploy.django_cdstack_deploy.func import (
    generate_config_static,
    zip_add_file,
)
from django_cdstack_tpl_cloudrouter.django_cdstack_tpl_cloudrouter.views import (
    handle_cloudrouter_tunnel_config,
)
from django_cdstack_tpl_deb_buster.django_cdstack_tpl_deb_buster.views import (
    handle as handle_deb_buster,
)
from django_cdstack_tpl_deb.django_cdstack_tpl_deb.views import handle as handle_deb
from django_cdstack_tpl_deb.django_cdstack_tpl_deb.views import get_os_release
from django_cdstack_tpl_deb_stretch.django_cdstack_tpl_deb_stretch.views import (
    get_apt_packages,
    get_apt_sources,
    get_apt_prefs,
)
from django_cdstack_tpl_dyndns_client.django_cdstack_tpl_dyndns_client.views import (
    handle as handle_dyndns_client,
)
from django_cdstack_tpl_crowdsec_main.django_cdstack_tpl_crowdsec_main.views import (
    handle as handle_crowdsec_main,
)
from django_cdstack_tpl_fail2ban.django_cdstack_tpl_fail2ban.views import (
    handle as handle_fail2ban,
)
from django_cdstack_tpl_ipcop.django_cdstack_tpl_ipcop.views import (
    handle as handle_ipcop,
)
from django_cdstack_tpl_shorewall.django_cdstack_tpl_shorewall.views import (
    handle as handle_shorewall,
)
from django_cdstack_tpl_uplink_failover.django_cdstack_tpl_uplink_failover.views import (
    handle as handle_uplink_failover,
)
from django_cdstack_tpl_wazuh_agent.django_cdstack_tpl_wazuh_agent.views import (
    handle as handle_wazuh_agent,
)
from django_cdstack_tpl_whclient.django_cdstack_tpl_whclient.views import (
    handle as handle_whclient,
)


def handle(zipfile_handler, template_opts, cmdb_host, skip_handle_os=False):
    django_engine = engines["django"]

    module_prefix = (
        "django_cdstack_tpl_cloudrouter_v2/django_cdstack_tpl_cloudrouter_v2"
    )

    if "apt_packages" not in template_opts:
        template_opts["apt_packages"] = get_apt_packages(
            module_prefix + "/templates/image/imageconf/vmtemplate.yml"
        )

    if "fail2ban_apps" not in template_opts:
        template_opts["fail2ban_apps"] = copy.deepcopy(template_opts["apt_packages"])

    if "apt_sources" not in template_opts:
        template_opts["apt_sources"] = get_apt_sources(
            module_prefix + "/templates/image/imageconf/vmtemplate.yml"
        )

    if "apt_preferences" not in template_opts:
        template_opts["apt_preferences"] = get_apt_prefs(
            module_prefix + "/templates/image/imageconf/vmtemplate.yml"
        )

    if "os_release" not in template_opts:
        template_opts["os_release"] = get_os_release(
            module_prefix + "/templates/image/imageconf/vmtemplate.yml"
        )

    # DNS Recursor handling
    pdns_recursor_forward_zones = list()

    for key in template_opts.keys():
        if key.startswith("pdns_recursor_forward_zone_") and key.endswith("_zone"):
            key_ident = key[:-5]

            pdns_recursor_forward_zone_forwarders = list()

            for key in template_opts.keys():
                if key.startswith(key_ident + "_forwarder_") and key.endswith("_ip"):
                    pdns_recursor_forward_zone_forwarders.append(template_opts[key])

            if len(pdns_recursor_forward_zone_forwarders) <= 0:
                pdns_recursor_forward_zone_forwarders.append(
                    template_opts["network_public_dns_server1"]
                )

            pdns_recursor_forward_zones.append(
                {
                    "zone": template_opts[key_ident + "_zone"],
                    "forwarders": pdns_recursor_forward_zone_forwarders,
                }
            )

    template_opts["pdns_recursor_forward_zones"] = pdns_recursor_forward_zones

    # IPsec handling
    ipsec_connections = list()

    for key in template_opts.keys():
        if key.startswith("ipsec_connection_") and key.endswith("_name"):
            key_ident = key[:-5]

            ipsec_connection = dict()

            ipsec_connection["ipsec_con_name"] = template_opts[key_ident + "_name"]

            if key_ident + "_fwzone" in template_opts:
                ipsec_connection["ipsec_con_fwzone"] = template_opts[
                    key_ident + "_fwzone"
                ]
            else:
                ipsec_connection["ipsec_con_fwzone"] = "net"

            if key_ident + "_authby" in template_opts:
                ipsec_connection["ipsec_con_authby"] = template_opts[
                    key_ident + "_authby"
                ]
            else:
                ipsec_connection["ipsec_con_authby"] = "secret"

            if key_ident + "_left" in template_opts:
                ipsec_connection["ipsec_con_left"] = template_opts[key_ident + "_left"]
            else:
                ipsec_connection["ipsec_con_left"] = "%defaultroute"

            if key_ident + "_leftid" in template_opts:
                ipsec_connection["ipsec_con_leftid"] = template_opts[
                    key_ident + "_leftid"
                ]
            else:
                ipsec_connection["ipsec_con_leftid"] = template_opts[
                    "network_iface_primary_ip"
                ]

            if key_ident + "_leftsubnet" in template_opts:
                ipsec_connection["ipsec_con_leftsubnet"] = template_opts[
                    key_ident + "_leftsubnet"
                ]
            else:
                ipsec_connection["ipsec_con_leftsubnet"] = "100.127.255.255/32"

            if key_ident + "_right" in template_opts:
                ipsec_connection["ipsec_con_right"] = template_opts[
                    key_ident + "_right"
                ]
            else:
                ipsec_connection["ipsec_con_right"] = "127.0.0.1"

            if key_ident + "_rightsubnet" in template_opts:
                ipsec_connection["ipsec_con_rightsubnet"] = template_opts[
                    key_ident + "_rightsubnet"
                ]
            else:
                ipsec_connection["ipsec_con_rightsubnet"] = "100.127.255.254/32"

            if key_ident + "_ike" in template_opts:
                ipsec_connection["ipsec_con_ike"] = template_opts[key_ident + "_ike"]
            else:
                ipsec_connection["ipsec_con_ike"] = "aes256-sha2_256-modp1024!"

            if key_ident + "_esp" in template_opts:
                ipsec_connection["ipsec_con_esp"] = template_opts[key_ident + "_esp"]
            else:
                ipsec_connection["ipsec_con_esp"] = "aes256-sha2_256!"

            if key_ident + "_keyingtries" in template_opts:
                ipsec_connection["ipsec_con_keyingtries"] = template_opts[
                    key_ident + "_keyingtries"
                ]
            else:
                ipsec_connection["ipsec_con_keyingtries"] = "%forever"

            if key_ident + "_ikelifetime" in template_opts:
                ipsec_connection["ipsec_con_ikelifetime"] = template_opts[
                    key_ident + "_ikelifetime"
                ]
            else:
                ipsec_connection["ipsec_con_ikelifetime"] = "1h"

            if key_ident + "_lifetime" in template_opts:
                ipsec_connection["ipsec_con_lifetime"] = template_opts[
                    key_ident + "_lifetime"
                ]
            else:
                ipsec_connection["ipsec_con_lifetime"] = "8h"

            if key_ident + "_dpddelay" in template_opts:
                ipsec_connection["ipsec_con_dpddelay"] = template_opts[
                    key_ident + "_dpddelay"
                ]
            else:
                ipsec_connection["ipsec_con_dpddelay"] = "30"

            if key_ident + "_dpdtimeout" in template_opts:
                ipsec_connection["ipsec_con_dpdtimeout"] = template_opts[
                    key_ident + "_dpdtimeout"
                ]
            else:
                ipsec_connection["ipsec_con_dpdtimeout"] = "120"

            if key_ident + "_dpdaction" in template_opts:
                ipsec_connection["ipsec_con_dpdaction"] = template_opts[
                    key_ident + "_dpdaction"
                ]
            else:
                ipsec_connection["ipsec_con_dpdaction"] = "restart"

            if key_ident + "_auto" in template_opts:
                ipsec_connection["ipsec_con_auto"] = template_opts[key_ident + "_auto"]
            else:
                ipsec_connection["ipsec_con_auto"] = "start"

            if key_ident + "_psk" in template_opts:
                ipsec_connection["ipsec_con_source"] = ipsec_connection[
                    "ipsec_con_leftid"
                ]
                ipsec_connection["ipsec_con_destination"] = ipsec_connection[
                    "ipsec_con_right"
                ]
                ipsec_connection["ipsec_con_psk"] = template_opts[key_ident + "_psk"]

            config_template_file_conf = open(
                module_prefix
                + "/templates/config-fs/dynamic/etc/ipsec.d/connection.conf",
                "r",
            ).read()
            config_template_conf = django_engine.from_string(config_template_file_conf)

            config_template_file_secrets = open(
                module_prefix
                + "/templates/config-fs/dynamic/etc/ipsec.d/connection.secrets",
                "r",
            ).read()
            config_template_secrets = django_engine.from_string(
                config_template_file_secrets
            )

            zip_add_file(
                zipfile_handler,
                "etc/ipsec.d/{}.conf".format(ipsec_connection["ipsec_con_name"]),
                config_template_conf.render(ipsec_connection),
            )

            zip_add_file(
                zipfile_handler,
                "etc/ipsec.d/{}.secrets".format(ipsec_connection["ipsec_con_name"]),
                config_template_secrets.render(ipsec_connection),
            )

            ipsec_connections.append(ipsec_connection)

    template_opts["ipsec_connections"] = ipsec_connections

    handle_cloudrouter_tunnel_config(
        zipfile_handler, template_opts, cmdb_host, skip_handle_os
    )
    generate_config_static(zipfile_handler, template_opts, module_prefix)

    handle_shorewall(zipfile_handler, template_opts, cmdb_host, skip_handle_os)

    handle_dyndns_client(zipfile_handler, template_opts, cmdb_host, skip_handle_os)
    handle_uplink_failover(zipfile_handler, template_opts, cmdb_host, skip_handle_os)
    # handle_openldap_proxy(zipfile_handler, template_opts, cmdb_host, skip_handle_os)
    handle_whclient(zipfile_handler, template_opts, cmdb_host, skip_handle_os)
    handle_ipcop(zipfile_handler, template_opts, cmdb_host, skip_handle_os)
    handle_fail2ban(zipfile_handler, template_opts, cmdb_host, skip_handle_os)
    handle_crowdsec_main(zipfile_handler, template_opts, cmdb_host, skip_handle_os)
    handle_wazuh_agent(zipfile_handler, template_opts, cmdb_host, skip_handle_os)

    if not skip_handle_os:
        handle_deb(zipfile_handler, template_opts, cmdb_host, skip_handle_os)

    return True
